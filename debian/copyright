Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: padbuster
Source: https://github.com/GDSSecurity/PadBuster

Files: *
Copyright: Brian Holyfield - Gotham Digital Science <labs@gdssecurity.com>
License: RPL1.5
 The Reciprocal Public License (RPL) is based on the concept of reciprocity or,
 if you prefer, fairness.
 .
 In short, this license grew out of a desire to close loopholes in previous open
 source licenses, loopholes that allowed parties to acquire open source software
 and derive financial benefit from it without having to release their
 improvements or derivatives to the community which enabled them. This occurred
 any time an entity did not release their application to a "third party".
 .
 While there is a certain freedom in this model of licensing, it struck the
 authors of the RPL as being unfair to the open source community at large and to
 the original authors of the works in particular. After all, bug fixes,
 extensions, and meaningful and valuable derivatives were not consistently
 finding their way back into the community where they could fuel further, and
 faster, growth and expansion of the overall open source software base.
 .
 While you should clearly read and understand the entire license, the essence of
 the RPL is found in two definitions: "Deploy" and "Required Components".
 .
 Regarding deployment, under the RPL your changes, bug fixes, extensions, etc.
 must be made available to the open source community at large when you Deploy in
 any form -- either internally or to an outside party. Once you start running
 the software you have to start sharing the software.
 .
 Further, under the RPL all components you author including schemas, scripts,
 source code, etc. -- regardless of whether they're compiled into a single
 binary or used as two halves of client/server application -- must be shared.
 You have to share the whole pie, not an isolated slice of it.
 .
 In addition to these goals, the RPL was authored to meet the requirements of
 the Open Source Definition as maintained by the Open Source Initiative (OSI).

Files: debian/*
Copyright: 2013 Devon Kearns <dookie@kali.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
